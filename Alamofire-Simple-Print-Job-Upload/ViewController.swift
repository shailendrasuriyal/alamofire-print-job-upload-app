//
//  ViewController.swift
//  Alamofire-Simple-Print-Job-Upload
//
//  Created by Shailendra Suriyal on 8/14/17.
//  Copyright © 2017 RealDev. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    @IBAction func uploadJob(_ sender: Any) {
        print("Shailu print");
//        let params: [String: String] = ["docAPIfunc": "DocPrint", "clientSWProtocol": "DocAPI", "clientSWKey": "7c67d3f7-9447-49b4-ba8c-ee28d641b17c", "clientSWName": "Print Anywhere", "clientSWVer": "1.0", "userLang": "en_US", "userEmail": "shailusuriyal@hp.com", "jobDestination": "900005228231", "userPassword": "123456", "documentURI": "www.yahoo.com"]
//
        let params: [String: String] = ["docAPIfunc": "DocPrint","clientSWProtocol": "DocAPI", "clientSWKey": "7c67d3f7-9447-49b4-ba8c-ee28d641b17c","clientSWName": "Print Anywhere", "clientSWVer": "1.0","userLang": "en_US", "userEmail": "shailusuriyal@hp.com", "jobDestination": "900005228231", "userPassword": "123456", "documentURI": "www.fun.com"]

//        Alamofire.request("https://demo1.printeronmopria.com/cps/DocApi",method: .post, parameters: params).responseString { (responses) in
//            print("Test \(responses)");
//        }

        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append("comment".data(using: .utf8)!, withName: "comment")
                multipartFormData.append("DocPrint".data(using: .utf8)!, withName: "docAPIfunc")
                multipartFormData.append("DocAPI".data(using: .utf8)!, withName: "clientSWProtocol")
                multipartFormData.append("7c67d3f7-9447-49b4-ba8c-ee28d641b17c".data(using: .utf8)!, withName: "clientSWKey")
                multipartFormData.append("Print Anywhere".data(using: .utf8)!, withName: "clientSWName")
                multipartFormData.append("1.0".data(using: .utf8)!, withName: "clientSWVer")
                multipartFormData.append("en_US".data(using: .utf8)!, withName: "userLang")
                multipartFormData.append("shailendrasuriyal@hp.com".data(using: .utf8)!, withName: "userEmail")
                multipartFormData.append("123456".data(using: .utf8)!, withName: "userPassword")
                multipartFormData.append("900005228231".data(using: .utf8)!, withName: "jobDestination")
                multipartFormData.append("www.google.com".data(using: .utf8)!, withName: "documentURI")
        },
            to: "https://demo1.printeronmopria.com/cps/DocApi",
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseString(completionHandler: { (responsess) in
                        print("shailu \(responsess)");
                    })
                case .failure(let encodingError):
                    print(encodingError)
                }
        }
        )
//
//        Alamofire.request("https://demo1.printeronmopria.com/cps/jobs",method: .get,headers: ["content-type": "application/json","Authorization": "Bearer 91bdbef5-fb84-4d38-a4f3-69204b2f8f60"]).responseJSON { (response) in
//                        print("testShailu \(response)");
//                    }
        
//        Alamofire.request("https://jsonplaceholder.typicode.com/posts", method: .get).responseJSON { (response) in
//            print("testShailu \(response)");
//        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

